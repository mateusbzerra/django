from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from .models import Posts,Owner

# Create your views here.
def index(request):
	owner = Owner.objects.get(id=1)
	post = Posts.objects.all()
	context = {'owner': owner,'posts':post}
	return render(request, 'news/index.html',context)
    #return HttpResponse("Olá mundo");

def post(request, post_id):
    #post = Posts.objects.get(id=post_id)
    try:
        post = Posts.objects.get(id=post_id)
    except Posts.DoesNotExist:
        return render(request, 'news/404.html')
    #post = get_object_or_404(Posts, id=post_id)
    owner = Owner.objects.get(id=1)
    return render(request, 'news/post.html', {'post': post,'owner':owner})

